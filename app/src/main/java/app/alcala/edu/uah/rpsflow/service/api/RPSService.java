package app.alcala.edu.uah.rpsflow.service.api;

import app.alcala.edu.uah.rpsflow.models.RPSPrediction;
import app.alcala.edu.uah.rpsflow.models.RPSPredictionRequest;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RPSService {

    public enum RESULTS {
        WON, LOST
    }


    @POST("predict")
    Call<RPSPrediction> predict(@Body RPSPredictionRequest request);

    @POST("graph")
    Call<ResponseBody> graph(@Body RPSPredictionRequest request);

}
