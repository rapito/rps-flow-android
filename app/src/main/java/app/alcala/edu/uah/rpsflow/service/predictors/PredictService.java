package app.alcala.edu.uah.rpsflow.service.predictors;

import app.alcala.edu.uah.rpsflow.models.Choice;
import app.alcala.edu.uah.rpsflow.models.GameResult;
import app.alcala.edu.uah.rpsflow.service.predictors.impl.RPSFlowBotPredictor;

public class PredictService implements RockPaperScissorMovePredictor {
    private static PredictService instance;
    private final RockPaperScissorMovePredictor predictor;

    public static PredictService instance() {
        return instance == null ? (instance = new PredictService()) : instance;
    }

    public PredictService() {
//        this.predictor = new RandomPredictor();
        this.predictor = new RPSFlowBotPredictor();
    }

    @Override
    public void predict(boolean isExtendedMode, RockPaperScissorMovePredictionListener listener) {
        this.predictor.predict(isExtendedMode, listener);
    }

    @Override
    public void clearTrainingData() {
        GameResult.deleteAll(GameResult.class);
        this.predictor.clearTrainingData();
    }

    @Override
    public void getGraph(boolean isExtendedMode, RockPaperScissorMovePredictionListener listener) {
        this.predictor.getGraph(isExtendedMode, listener);
    }

    @Override
    public void train(boolean won, Choice playersChoice, Choice predictorsChoice) {
        GameResult.create(!won, won, playersChoice.move.ordinal(), predictorsChoice.move.ordinal());
        this.predictor.train(won, playersChoice, predictorsChoice);
    }
}
