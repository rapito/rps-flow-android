package app.alcala.edu.uah.rpsflow;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import static android.os.Environment.DIRECTORY_DCIM;

public class Util {
    private static ProgressDialog progressDialog;

    public static Integer[] toArray(List<Integer> list) {
        Integer[] res = new Integer[list.size()];
        for (int i = 0; i < list.size(); i++) {
            res[i] = list.get(i);
        }
        return res;
    }

    public static void showLoading(Context context) {
        Util.showLoading(context, "Please Wait");
    }

    public static void showLoading(Context context, int stringId) {
        Util.showLoading(context, context.getString(stringId));
    }

    public static void showLoading(Context context, String message) {
        Util.hideLoading();
        ProgressDialog dialog = getProgressDialog(context);
        dialog.setIndeterminate(true);
        dialog.setMessage(message);
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void setLoadingCancelable() {
        if (Util.progressDialog == null) return;
        try {
            Util.progressDialog.setCancelable(true);
        } catch (Exception ignored) {
            Log.e(Util.class.getName(), "Error setting dialog as cancelable", ignored);
        }
    }

    public static void hideLoading() {
        if (Util.progressDialog == null) return;

        try {
            Util.progressDialog.dismiss();
        } catch (Exception ignored) {
            Log.e(Util.class.getName(), "Error hiding loading", ignored);
        }
        Util.progressDialog = null;
    }

    public static void showAlert(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setMessage(message);

        if (title == null || title.length() == 0) {
            builder.setTitle(title);
        }
        builder.create().show();
    }

    public static ProgressDialog getProgressDialog(Context context) {
        return (Util.progressDialog = new ProgressDialog(context));
    }

    public static void showImage(final Activity activity, byte[] graph) {
        View v = activity.getLayoutInflater().inflate(R.layout.image_layout, null);

        ImageView iv = v.findViewById(R.id.image);

        final Bitmap bitmap = BitmapFactory.decodeByteArray(graph, 0, graph.length);
        Drawable image = new BitmapDrawable(activity.getResources(), bitmap);
        iv.setImageDrawable(image);

        Dialog settingsDialog = new Dialog(activity);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(v);
        settingsDialog.show();
    }
}
