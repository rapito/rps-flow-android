package app.alcala.edu.uah.rpsflow.models;

import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import static app.alcala.edu.uah.rpsflow.service.predictors.RockPaperScissorMovePredictor.MOVES;

public class GameResult extends SugarRecord {
    public boolean playerWon;
    public boolean opponentWon;
    public int playerMove;
    public int opponentMove;

    public GameResult() {
    }

    public GameResult(boolean playerWon, boolean opponentWon, int playerMove, int opponentMove) {
        this.playerWon = playerWon;
        this.opponentWon = opponentWon;
        this.playerMove = playerMove;
        this.opponentMove = opponentMove;
    }

    public static void create(boolean playerWon, boolean opponentWon, int playerMove, int opponentMove) {
        GameResult r = new GameResult(playerWon, opponentWon, playerMove, opponentMove);
        r.save();
    }

    public static GameResult random() {
        Random r = new Random();
        List<GameResult> results = new ArrayList<>();

        results.add(new GameResult(true, false, MOVES.ROCK.ordinal(), MOVES.SCISSORS.ordinal()));
        results.add(new GameResult(true, false, MOVES.SCISSORS.ordinal(), MOVES.PAPER.ordinal()));
        results.add(new GameResult(true, false, MOVES.SCISSORS.ordinal(), MOVES.SCISSORS.ordinal()));
        results.add(new GameResult(true, false, MOVES.ROCK.ordinal(), MOVES.ROCK.ordinal()));
        results.add(new GameResult(true, false, MOVES.SCISSORS.ordinal(), MOVES.PAPER.ordinal()));
        results.add(new GameResult(false, true, MOVES.PAPER.ordinal(), MOVES.SCISSORS.ordinal()));
        results.add(new GameResult(true, false, MOVES.SCISSORS.ordinal(), MOVES.PAPER.ordinal()));
        results.add(new GameResult(false, true, MOVES.SCISSORS.ordinal(), MOVES.ROCK.ordinal()));
        results.add(new GameResult(true, false, MOVES.SCISSORS.ordinal(), MOVES.SCISSORS.ordinal()));
        results.add(new GameResult(true, false, MOVES.ROCK.ordinal(), MOVES.SCISSORS.ordinal()));
        results.add(new GameResult(false, true, MOVES.SCISSORS.ordinal(), MOVES.ROCK.ordinal()));
        results.add(new GameResult(true, false, MOVES.PAPER.ordinal(), MOVES.PAPER.ordinal()));

        return results.get(r.nextInt(results.size()));
    }

    public static List<GameResult> all() {
        List<GameResult> res = new ArrayList<>();
        Iterator<GameResult> iterator = findAll(GameResult.class);
        while (iterator.hasNext()) {
            res.add(iterator.next());
        }
        return res;
    }
}
