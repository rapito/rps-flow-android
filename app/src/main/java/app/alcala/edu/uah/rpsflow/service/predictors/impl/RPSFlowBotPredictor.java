package app.alcala.edu.uah.rpsflow.service.predictors.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import app.alcala.edu.uah.rpsflow.RPSFlowApplication;
import app.alcala.edu.uah.rpsflow.Util;
import app.alcala.edu.uah.rpsflow.models.Choice;
import app.alcala.edu.uah.rpsflow.models.GameResult;
import app.alcala.edu.uah.rpsflow.models.Prediction;
import app.alcala.edu.uah.rpsflow.models.RPSPrediction;
import app.alcala.edu.uah.rpsflow.models.RPSPredictionRequest;
import app.alcala.edu.uah.rpsflow.service.api.API;
import app.alcala.edu.uah.rpsflow.service.api.RPSService;
import app.alcala.edu.uah.rpsflow.service.predictors.RockPaperScissorMovePredictionListener;
import app.alcala.edu.uah.rpsflow.service.predictors.RockPaperScissorMovePredictor;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RPSFlowBotPredictor implements RockPaperScissorMovePredictor, Callback<RPSPrediction> {

    private static final int MAX_RESULTS = 500;
    private RockPaperScissorMovePredictionListener listener;

    @Override
    public void predict(boolean isExtendedMode, RockPaperScissorMovePredictionListener listener) {
        this.listener = listener;

        RPSPredictionRequest request = buildRequest(isExtendedMode);
        API.get(RPSFlowApplication.context).predict(request).enqueue(this);
    }

    private RPSPredictionRequest buildRequest(boolean isExtendedMode) {
        RPSPredictionRequest request = new RPSPredictionRequest();
        List<Integer[]> data = new ArrayList<>();
        List<Integer> labels = new ArrayList<>();

        List<GameResult> all = GameResult.all();
        List<GameResult> results = all;

        if (all.size() > MAX_RESULTS) {
            results = all.subList(all.size() - MAX_RESULTS, all.size());
        }

        List<GameResult> toRemove = new ArrayList<>();

        if (!isExtendedMode) {
            for (GameResult gr : results) {
                if (gr.opponentMove > MOVES.SCISSORS.ordinal() || gr.playerMove > MOVES.SCISSORS.ordinal())
                    toRemove.add(gr);
            }
        }

        for (GameResult g : toRemove) {
            results.remove(g);
        }

        for (int i = 0; i < results.size(); i++) {

            GameResult g1 = results.get(i);
            GameResult g2 = null;

            if (i + 1 < results.size()) {
                g2 = results.get(i + 1);
            } else {
                g2 = GameResult.random();
            }

            data.add(createArrayFromGameResults(g1, g2));
            labels.add(createLabelFromGameResult(g2, isExtendedMode));
        }

        request.setData(data);
        request.setLabels(Util.toArray(labels));
        return request;
    }

    private Integer createLabelFromGameResult(GameResult result, boolean isExtendedMode) {
        MOVES move = null;

        switch (result.opponentMove) {
            case 0: // Rock
                move = isExtendedMode ? random(MOVES.PAPER, MOVES.SPOCK) : MOVES.PAPER;

                break;
            case 1: // Paper
                move = isExtendedMode ? random(MOVES.SCISSORS, MOVES.LIZARD) : MOVES.SCISSORS;

                break;
            case 2: // Scissors
                move = isExtendedMode ? random(MOVES.ROCK, MOVES.SPOCK) : MOVES.ROCK;

                break;
            case 3: // Lizard
                move = random(MOVES.ROCK, MOVES.SCISSORS);
                break;
            case 4: // Spock
                move = random(MOVES.PAPER, MOVES.LIZARD);
                break;
        }

        return move.ordinal();
    }

    private MOVES random(MOVES m1, MOVES m2) {
        if (new Random().nextInt(11) > 5) {
            return m1;
        } else {
            return m2;
        }
    }


    private Integer[] createArrayFromGameResults(GameResult g1, GameResult g2) {
        ArrayList<Integer> res = new ArrayList<>();

        res.add(g1.opponentWon ? RPSService.RESULTS.WON.ordinal() : RPSService.RESULTS.LOST.ordinal());
        res.add(g1.playerMove);
        res.add(g1.opponentMove);
        res.add(g2.opponentWon ? RPSService.RESULTS.WON.ordinal() : RPSService.RESULTS.LOST.ordinal());
        res.add(g2.playerMove);
        res.add(g2.opponentMove);

        return Util.toArray(res);
    }

    @Override
    public void clearTrainingData() {

    }

    @Override
    public void getGraph(boolean isExtendedMode, final RockPaperScissorMovePredictionListener listener) {
        API.get(RPSFlowApplication.context).graph(buildRequest(isExtendedMode)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call call, Response response) {
                try {
                    ResponseBody body = (ResponseBody) response.body();
                    listener.onGraphResult(body.bytes());
                } catch (IOException e) {
                    listener.onGraphResult(null);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                listener.onGraphResult(null);
            }
        });
    }

    @Override
    public void train(boolean won, Choice playersChoice, Choice predictorsChoice) {

    }

    @Override
    public void onResponse(Call<RPSPrediction> call, Response<RPSPrediction> response) {
        Prediction p = new Prediction(response.body());
        this.listener.onRockPaperScissorMovePredicted(p);
    }

    @Override
    public void onFailure(Call<RPSPrediction> call, Throwable t) {
        new RandomPredictor().predict(false, this.listener);
    }
}
