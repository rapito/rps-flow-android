package app.alcala.edu.uah.rpsflow.service.predictors.impl;

import java.util.Random;

import app.alcala.edu.uah.rpsflow.models.Choice;
import app.alcala.edu.uah.rpsflow.models.Prediction;
import app.alcala.edu.uah.rpsflow.service.predictors.RockPaperScissorMovePredictionListener;
import app.alcala.edu.uah.rpsflow.service.predictors.RockPaperScissorMovePredictor;

public class RandomPredictor implements RockPaperScissorMovePredictor {
    private final Random r;

    public RandomPredictor() {
        this.r = new Random();
    }

    @Override
    public void predict(boolean isExtendedMode, RockPaperScissorMovePredictionListener listener) {
        Prediction p = new Prediction(MOVES.values()[r.nextInt(MOVES.values().length)]);
        listener.onRockPaperScissorMovePredicted(p);
    }

    @Override
    public void clearTrainingData() {
    }

    @Override
    public void getGraph(boolean isExtendedMode, RockPaperScissorMovePredictionListener listener) {
        listener.onGraphResult(null);
    }

    @Override
    public void train(boolean won, Choice playersChoice, Choice predictorsChoice) {
    }
}
