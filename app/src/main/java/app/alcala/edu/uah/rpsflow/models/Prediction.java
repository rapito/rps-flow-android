package app.alcala.edu.uah.rpsflow.models;

import static app.alcala.edu.uah.rpsflow.service.predictors.RockPaperScissorMovePredictor.MOVES;

public class Prediction {
    public MOVES move;

    public Prediction(RPSPrediction rpsPrediction) {
            this(MOVES.values()[rpsPrediction.move]);
    }

    public Prediction(MOVES move) {
        this.move = move;
    }
}
