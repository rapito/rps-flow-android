package app.alcala.edu.uah.rpsflow.service.api;

import android.content.Context;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;

import app.alcala.edu.uah.rpsflow.BuildConfig;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class API {
    private static final String API_HOST = BuildConfig.API_HOST;

    private Context context;
    private static API instance;
    private final RPSService service;

    public API(Context context) {
        this.context = context;
        this.service = buildService();
    }

    private RPSService buildService() {
        RPSService service;

        final OkHttpClient client = new OkHttpClient.Builder()
                .build();


        Retrofit retrofit = buildRetroFit(API.API_HOST.concat("/"), client);
        service = retrofit.create(RPSService.class);

        return service;
    }

    private static Retrofit buildRetroFit(String baseUrl, OkHttpClient client) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setLongSerializationPolicy(LongSerializationPolicy.DEFAULT)
                .create();

        Retrofit.Builder builder = new Retrofit.Builder();

        if (client != null) builder.client(client);

        builder.baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson));

        return builder.build();

    }

    private static API getInstance(Context context) {

        if (instance != null) instance.context = context;
        return instance == null ? (instance = new API(context)) : instance;
    }

    public static RPSService get(Context context) {
        return getInstance(context).service;
    }
}
