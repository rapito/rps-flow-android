package app.alcala.edu.uah.rpsflow;

import android.app.Application;

import com.orm.SugarContext;

public class RPSFlowApplication extends Application {
    public static RPSFlowApplication context;

    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(this);
        context = this;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }
}
