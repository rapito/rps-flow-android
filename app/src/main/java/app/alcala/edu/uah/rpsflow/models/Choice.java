package app.alcala.edu.uah.rpsflow.models;

import android.view.View;

import app.alcala.edu.uah.rpsflow.service.predictors.RockPaperScissorMovePredictor;

public class Choice {
    public final RockPaperScissorMovePredictor.MOVES move;
    public final View button;

    public Choice(RockPaperScissorMovePredictor.MOVES move, View button) {
        this.move = move;
        this.button = button;
    }
}
