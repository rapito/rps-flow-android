package app.alcala.edu.uah.rpsflow.models;

public class RPSPrediction {
    public Integer move;
    public String moveName;

    public RPSPrediction(Integer move, String moveName) {
        this.move = move;
        this.moveName = moveName;
    }


    public Integer getMove() {
        return move;
    }

    public void setMove(Integer move) {
        this.move = move;
    }

    public String getMoveName() {
        return moveName;
    }

    public void setMoveName(String moveName) {
        this.moveName = moveName;
    }
}
