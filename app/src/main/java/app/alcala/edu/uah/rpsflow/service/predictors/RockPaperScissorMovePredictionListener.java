package app.alcala.edu.uah.rpsflow.service.predictors;

import app.alcala.edu.uah.rpsflow.models.Prediction;

public interface RockPaperScissorMovePredictionListener {

    void onRockPaperScissorMovePredicted(Prediction p);

    void onGraphResult(byte[] graph);
}
