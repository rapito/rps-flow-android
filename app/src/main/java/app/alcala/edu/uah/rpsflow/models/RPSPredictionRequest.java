package app.alcala.edu.uah.rpsflow.models;

import java.util.List;

public class RPSPredictionRequest {
    public List<Integer[]> data;
    public Integer[] labels;

    public Integer[] getLabels() {
        return labels;
    }

    public void setLabels(Integer[] labels) {
        this.labels = labels;
    }

    public RPSPredictionRequest(List<Integer[]> data, Integer[] labels) {

        this.data = data;
        this.labels = labels;
    }

    public RPSPredictionRequest(List<Integer[]> data) {
        this.data = data;
    }

    public RPSPredictionRequest() {
    }

    public List<Integer[]> getData() {
        return data;
    }

    public void setData(List<Integer[]> data) {
        this.data = data;
    }

}
