package app.alcala.edu.uah.rpsflow;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.iconics.context.IconicsContextWrapper;
import com.ufreedom.floatingview.Floating;
import com.ufreedom.floatingview.FloatingBuilder;
import com.ufreedom.floatingview.FloatingElement;
import com.ufreedom.floatingview.effect.ScaleFloatingTransition;

import app.alcala.edu.uah.rpsflow.models.Choice;
import app.alcala.edu.uah.rpsflow.models.Prediction;
import app.alcala.edu.uah.rpsflow.service.predictors.PredictService;
import app.alcala.edu.uah.rpsflow.service.predictors.RockPaperScissorMovePredictionListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static app.alcala.edu.uah.rpsflow.service.predictors.RockPaperScissorMovePredictor.MOVES;
import static app.alcala.edu.uah.rpsflow.service.predictors.RockPaperScissorMovePredictor.PLAYERS;

public class MainActivity extends AppCompatActivity implements RockPaperScissorMovePredictionListener {

    private static final long DURATION_ANIMATION = 3500;
    @BindView(R.id.text_score_opponent)
    public TextView opponentScoreTextView;
    @BindView(R.id.text_score_player)
    public TextView playerScoreTextView;

    @BindView(R.id.text_move_opponent)
    public TextView opponentMoveTextView;
    @BindView(R.id.text_move_player)
    public TextView playerMoveTextView;

    @BindView(R.id.btn_rock)
    FloatingActionButton rockButton;
    @BindView(R.id.btn_paper)
    FloatingActionButton paperButton;
    @BindView(R.id.btn_scissors)
    FloatingActionButton scissorButton;

    @BindView(R.id.btn_rock_opponent)
    FloatingActionButton opponentRockButton;
    @BindView(R.id.btn_paper_opponent)
    FloatingActionButton opponentPaperButton;
    @BindView(R.id.btn_scissors_opponent)
    FloatingActionButton opponentScissorButton;

    @BindView(R.id.btn_lizard)
    FloatingActionButton lizardButton;
    @BindView(R.id.btn_spock)
    FloatingActionButton spockButton;

    @BindView(R.id.btn_lizard_opponent)
    FloatingActionButton opponentSerpentButton;
    @BindView(R.id.btn_spock_opponent)
    FloatingActionButton opponentSpockButton;

    private Integer opponentScore;
    private Integer playerScore;

    private Choice opponentsChoice;
    private Choice playersChoice;
    private Floating floating;

    public int MAX_SCORE = 5;
    private boolean loading = false;
    private boolean inExtendedMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        this.opponentScore = 0;
        this.playerScore = 0;
        this.floating = new Floating(this);
        setToNormalMode();
    }

    @Override
    protected void onResume() {
        super.onResume();
        playAsOpponent();
    }

    @OnClick({R.id.btn_paper, R.id.btn_rock, R.id.btn_scissors, R.id.btn_lizard, R.id.btn_spock})
    public void onButtonClick(View button) {
        if (this.loading) {
            animateButton("LET ME THINK!", true, button, true);
            Toast.makeText(this, "Pre-loading prediction from server, Try again.", Toast.LENGTH_SHORT).show();
            return;
        }
        chooseMove(PLAYERS.PLAYER, getMoveFromButton(button.getId()), button);
    }

    @OnClick({R.id.btn_reset, R.id.btn_clear, R.id.btn_graph, R.id.btn_extend})
    public void onExtrasClick(View button) {
        String text = null;
        switch (button.getId()) {
            case R.id.btn_reset:
                text = "Game Reset";
                break;
            case R.id.btn_clear:
                text = "DB Cleared";
                break;
            case R.id.btn_graph:
                text = "Downloading Graph!";
                break;
            case R.id.btn_extend:
                text = this.inExtendedMode ? "Normal Mode!" : "Extended Mode!";
                break;
        }
        animateButton(text, false, button, true);
    }

    @OnClick(R.id.btn_graph)
    public void onGraphClicked(View button) {
        downloadGraph();
    }

    @OnClick(R.id.btn_extend)
    public void onExtendClicked(View button) {
        toggleGameMode();
    }

    @OnClick(R.id.btn_reset)
    public void onRestartClick(View button) {
        clearScores();
    }

    @OnClick(R.id.btn_clear)
    public void onClearDBClick(View button) {
        clearScores();
        PredictService.instance().clearTrainingData();
    }

    private void setToExtendedMode() {
        int fabSize = FloatingActionButton.SIZE_MINI;
        int visibility = View.VISIBLE;

        this.opponentSerpentButton.setVisibility(visibility);
        this.opponentSpockButton.setVisibility(visibility);

        this.lizardButton.setVisibility(visibility);
        this.spockButton.setVisibility(visibility);

        this.opponentRockButton.setSize(fabSize);
        this.opponentPaperButton.setSize(fabSize);
        this.opponentScissorButton.setSize(fabSize);

        this.rockButton.setSize(fabSize);
        this.paperButton.setSize(fabSize);
        this.scissorButton.setSize(fabSize);

        this.inExtendedMode = true;
    }

    private void setToNormalMode() {
        int fabSize = FloatingActionButton.SIZE_NORMAL;
        int visibility = View.GONE;

        this.opponentSerpentButton.setVisibility(visibility);
        this.opponentSpockButton.setVisibility(visibility);

        this.lizardButton.setVisibility(visibility);
        this.spockButton.setVisibility(visibility);

        this.opponentRockButton.setSize(fabSize);
        this.opponentPaperButton.setSize(fabSize);
        this.opponentScissorButton.setSize(fabSize);

        this.rockButton.setSize(fabSize);
        this.paperButton.setSize(fabSize);
        this.scissorButton.setSize(fabSize);

        this.inExtendedMode = false;
    }

    private void toggleGameMode() {
        if (this.inExtendedMode) {
            setToNormalMode();
        } else {
            setToExtendedMode();
        }

    }

    private void downloadGraph() {
        Util.showLoading(this);
        PredictService.instance().getGraph(this.inExtendedMode, this);
    }

    private void chooseMove(PLAYERS player, MOVES move, View button) {
        switch (player) {
            case OPPONENT:
                if (this.opponentsChoice != null) return;
                this.opponentsChoice = new Choice(move, button);
                break;
            case PLAYER:
                if (this.playersChoice != null) return;
                this.playersChoice = new Choice(move, button);
                playAsOpponent();
                break;
        }


        if (this.opponentsChoice != null && this.playersChoice != null) {
            onBothPlayersPlayed(this.playersChoice, this.opponentsChoice);
        }
    }

    private void clearScores() {
        updateScore(PLAYERS.PLAYER, -this.playerScore);
        updateScore(PLAYERS.OPPONENT, -this.opponentScore);
        this.opponentMoveTextView.setText("");
        this.playerMoveTextView.setText("");
    }

    public synchronized void onBothPlayersPlayed(Choice playersChoice, Choice opponentsChoice) {
        View playerButton = playersChoice.button;
        View opponentButton = opponentsChoice.button;
        MOVES playerMove = playersChoice.move;
        MOVES opponentMove = opponentsChoice.move;

        PLAYERS winner = getWinner(playerMove, opponentMove);
        animateButtons(PLAYERS.PLAYER, playerButton, playerMove, PLAYERS.PLAYER == winner);
        animateButtons(PLAYERS.OPPONENT, opponentButton, opponentMove, PLAYERS.OPPONENT == winner);
        updateScore(winner, 1);

        PredictService.instance().train(PLAYERS.OPPONENT == winner, playersChoice, opponentsChoice);

        this.opponentMoveTextView.setText(opponentsChoice.move.name());
        this.playerMoveTextView.setText(playersChoice.move.name());

        this.playersChoice = null;
        this.opponentsChoice = null;

        checkGame();
    }

    private void animateButtons(PLAYERS player, View button, MOVES moves, Boolean isWinner) {
        animateButton(moves.name() + "!", isWinner, button, player == PLAYERS.PLAYER);
    }

    private void animateButton(String text, Boolean bold, View button, Boolean up) {
        TextView targetView = new TextView(MainActivity.this);
        targetView.setText(text);
        if (bold) {
            targetView.setTypeface(null, Typeface.BOLD);
        }

        ScaleFloatingTransition transition = new ScaleFloatingTransition(DURATION_ANIMATION);

        FloatingElement floatingElement = new FloatingBuilder()
                .anchorView(button)
                .targetView(targetView)
                .offsetY(button.getMeasuredHeight() * (up ? -1 : 1))
                .floatingTransition(transition)
                .build();

        this.floating.startFloating(floatingElement);
    }

    private void updateScore(PLAYERS winner, int inc) {
        if (winner == null) return;
        TextView v = null;
        Integer score = null;

        switch (winner) {
            case OPPONENT:
                v = this.opponentScoreTextView;
                score = (this.opponentScore += inc);
                break;
            case PLAYER:
                v = this.playerScoreTextView;
                score = (this.playerScore += inc);
                break;
        }

        v.setText(String.valueOf(score));
    }

    private PLAYERS getWinner(MOVES playerMove, MOVES opponentMove) {
        if (playerMove.equals(opponentMove)) return null;

        PLAYERS winner = null;

        switch (playerMove) {
            case ROCK:
                switch (opponentMove) {
                    case LIZARD:
                    case SCISSORS:
                        winner = PLAYERS.PLAYER;
                        break;
                    case SPOCK:
                    case PAPER:
                        winner = PLAYERS.OPPONENT;
                        break;
                }
                break;
            case PAPER:
                switch (opponentMove) {
                    case ROCK:
                    case SPOCK:
                        winner = PLAYERS.PLAYER;
                        break;
                    case SCISSORS:
                    case LIZARD:
                        winner = PLAYERS.OPPONENT;
                        break;
                }
                break;
            case SCISSORS:
                switch (opponentMove) {
                    case LIZARD:
                    case PAPER:
                        winner = PLAYERS.PLAYER;
                        break;
                    case ROCK:
                    case SPOCK:
                        winner = PLAYERS.OPPONENT;
                        break;
                }
                break;
            case LIZARD:
                switch (opponentMove) {
                    case SPOCK:
                    case PAPER:
                        winner = PLAYERS.PLAYER;
                        break;
                    case ROCK:
                    case SCISSORS:
                        winner = PLAYERS.OPPONENT;
                        break;
                }
                break;
            case SPOCK:
                switch (opponentMove) {
                    case ROCK:
                    case SCISSORS:
                        winner = PLAYERS.PLAYER;
                        break;
                    case PAPER:
                    case LIZARD:
                        winner = PLAYERS.OPPONENT;
                        break;
                }
                break;
        }

        return winner;
    }

    private void playAsOpponent() {
        startLoading();
        PredictService.instance().predict(this.inExtendedMode, this);
    }

    public void onRockPaperScissorMovePredicted(Prediction prediction) {
        stopLoading();
        chooseMove(PLAYERS.OPPONENT, prediction.move, findOpponentButton(prediction.move));
    }

    @Override
    public void onGraphResult(byte[] graph) {
        Util.hideLoading();
        if (graph == null) return;
        Util.showImage(this, graph);
    }

    private void startLoading() {
        this.loading = true;
//        Util.showLoading(this);
    }

    private void stopLoading() {
        this.loading = false;
//        Util.hideLoading();
    }

    private void checkGame() {
        if (this.opponentScore != MAX_SCORE && this.playerScore != MAX_SCORE) return;

        PLAYERS winner = null;
        if (this.opponentScore == MAX_SCORE) {
            winner = PLAYERS.OPPONENT;
        } else {
            winner = PLAYERS.PLAYER;
        }

        onGameEnded(winner);
    }

    public void onGameEnded(PLAYERS winner) {
        String message = null;

        switch (winner) {
            case PLAYER:
                message = "You Won!";
                break;
            case OPPONENT:
                message = "You Lost!";
                break;
        }

        Util.showAlert(this, "GAME", message);
        clearScores();
    }

    private View findOpponentButton(MOVES move) {
        Integer id = R.id.btn_paper_opponent;

        switch (move) {
            case PAPER:
                id = R.id.btn_paper_opponent;
                break;
            case SCISSORS:
                id = R.id.btn_scissors_opponent;
                break;
            case ROCK:
                id = R.id.btn_rock_opponent;
                break;
            case LIZARD:
                id = R.id.btn_lizard_opponent;
                break;
            case SPOCK:
                id = R.id.btn_spock_opponent;
                break;
        }


        return findViewById(id);
    }

    private MOVES getMoveFromButton(int id) {
        switch (id) {
            case R.id.btn_rock:
            case R.id.btn_rock_opponent:
                return MOVES.ROCK;
            case R.id.btn_paper:
            case R.id.btn_paper_opponent:
                return MOVES.PAPER;
            case R.id.btn_scissors:
            case R.id.btn_scissors_opponent:
                return MOVES.SCISSORS;
            case R.id.btn_lizard:
            case R.id.btn_lizard_opponent:
                return MOVES.LIZARD;
            case R.id.btn_spock:
            case R.id.btn_spock_opponent:
                return MOVES.SPOCK;
        }
        return MOVES.ROCK;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }

}
