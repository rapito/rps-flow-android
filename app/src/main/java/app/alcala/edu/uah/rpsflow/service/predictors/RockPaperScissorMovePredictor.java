package app.alcala.edu.uah.rpsflow.service.predictors;

import app.alcala.edu.uah.rpsflow.models.Choice;

public interface RockPaperScissorMovePredictor {

    void predict(boolean isExtendedMode, RockPaperScissorMovePredictionListener listener);

    void clearTrainingData();

    void getGraph(boolean isExtendedMode, RockPaperScissorMovePredictionListener listener);

    enum MOVES {
        ROCK, PAPER, SCISSORS, LIZARD, SPOCK
    }

    enum PLAYERS {
        PLAYER, OPPONENT
    }

    void train(boolean won, Choice playersChoice, Choice predictorsChoice);
}
